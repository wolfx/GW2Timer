/**
 * 将秒转换成时间字符
 * @param {Object} value
 */
function formatSeconds(value) {
	var theTime = parseInt(value);
	var theTime1 = 0;
	var theTime2 = 0;

	if(theTime >= 60) {
		theTime1 = parseInt(theTime / 60);
		theTime = parseInt(theTime % 60);
		if(theTime1 >= 60) {
			theTime2 = parseInt(theTime1 / 60);
			theTime1 = parseInt(theTime1 % 60);
		}
	}
	if(theTime < 10) {
		theTime = "0" + parseInt(theTime);
	}
	var result = "" + theTime + "";
	if(theTime1 >= 0) {
		if(theTime1 < 10) {
			theTime1 = "0" + parseInt(theTime1);
		}
		result = "" + theTime1 + ":" + result;
	}
	if(theTime2 >= 0) {
		if(theTime2 < 10) {
			theTime2 = "0" + parseInt(theTime2);
		}
		result = "" + theTime2 + ":" + result;
	}
	return result;
}

/**
 * 将时间字符转换成秒
 * @param {Object} v_time
 */
function formatTimeToSeconds(v_time) {
	var t_arr = v_time.split(":");
	var hh = t_arr[0];
	var mm = t_arr[1];
	var ss = t_arr[2];
	var s_now = parseInt(hh * 3600) + parseInt(mm * 60) + parseInt(ss);
	return s_now;
}

function formatTimeNum(n) {
	if(n < 10) {
		n = '0' + n;
	} else {
		n = '' + n;
	}
	return n;
}

function formatTimeToStr(t) {
	var h = formatTimeNum(t.getHours());
	var m = formatTimeNum(t.getMinutes());
	var s = formatTimeNum(t.getSeconds());
	return h + ":" + m + ":" + s;
}

function dayOrNight(nowTime) {
	// 双数点30开始到单数点45 是白天
	var h = +nowTime.split(':')[0];
	var m = +nowTime.split(':')[1];
	var condition_0 = h % 2 == 0 && m > 30; // 时间大于双数30
	var condition_1 = h % 2 != 0 && m < 45; // 时间小于单数45
	if(condition_0 || condition_1) {
		if(h % 2 == 0) {
			return '现在是白天，' + (h + 1) + ':45:00进入黑夜';
		} else {
			return '现在是白天，' + h + ':45:00进入黑夜';
		}
	} else {
		if(h % 2 == 0) {
			return '现在是黑夜，' + h + ':30:00进入白天';
		} else {
			return '现在是黑夜，' + (h + 1) + ':30:00进入白天';
		}
	}

}

/**
 * 时间表生成器
 * @param {Object} startTime 开始时间
 * @param {Object} step 步进（小时）
 */
function timeListGenerator(startTime, step) {
	var startSec = formatTimeToSeconds(startTime);
	var maxSec = formatTimeToSeconds('24:00:00');
	var stepSec = step * 3600;
	var result = [];
	for(var i = startSec; i < maxSec; i += stepSec) {
		result.push(formatSeconds(i));
	}
	return result;
}
