var dailyLocation = [
	{
		name: "科瑞塔",
		children: [{
				name: "景观点",
				waypoint: {
					name: "科瑞塔传送点",
					chatlink: "[&BPgAAAA=]"
				},
				direction: "9点"
			},
			{
				name: "采矿者",
				waypoint: {
					name: "菲尼传送点",
					chatlink: "[&BPMAAAA=]"
				},
				direction: "12点山洞，入口在左边"
			},
			{
				name: "伐木者",
				waypoint: {
					name: "阿卡传送点",
					chatlink: "[&BLIAAAA=]"
				},
				direction: "4-6点共四组"
			},
			{
				name: "收割者",
				waypoint: {
					name: "甲虫郡传送点",
					chatlink: "[&BPoAAAA=]"
				},
				direction: "7点"
			},
		]
	},
	{
		name: "席瓦雪山",
		children: [{
				name: "景观点",
				waypoint: {
					name: "南部哨站传送点",
					chatlink: "[&BI4DAAA=]"
				},
				direction: "12点"
			},
			{
				name: "采矿者",
				waypoint: {
					name: "帷幔城垛传送点",
					chatlink: "[&BEwCAAA=]"
				},
				direction: "11点，矿+微远富矿,富矿入口在收割3点水下"
			},
			{
				name: "伐木者",
				waypoint: {
					name: "毒蛇传送点",
					chatlink: "[&BEUCAAA=]"
				},
				direction: "传送点附近一圈，四组"
			},
			{
				name: "收割者",
				waypoint: {
					name: "帷幔城垛传送点",
					chatlink: "[&BEwCAAA=]"
				},
				direction: "11点微远"
			},
		]
	},
	{
		name: "阿斯卡隆",
		children: [{
				name: "景观点",
				waypoint: {
					name: "孤斗士传送点",
					chatlink: "[&BKQDAAA=]"
				},
				direction: "右手出门后左转"
			},
			{
				name: "采矿者",
				waypoint: {
					name: "斯克尔营地传送点",
					chatlink: "[&BEoBAAA=]"
				},
				direction: "9点富矿+1点矿"
			},
			{
				name: "伐木者",
				waypoint: {
					name: "无望之门传送点",
					chatlink: "[&BFABAAA=]"
				},
				direction: "1点"
			},
			{
				name: "收割者",
				waypoint: {
					name: "钳爪传送点",
					chatlink: "[&BMcDAAA=]"
				},
				direction: "6点"
			},
		]
	},
	{
		name: "迈古玛丛林",
		children: [{
				name: "景观点",
				waypoint: {
					name: "卡勒顿避难所传送点",
					chatlink: "[&BDwBAAA=]"
				},
				direction: ""
			},
			{
				name: "采矿者",
				waypoint: {
					name: "盗匪荒地传送点",
					chatlink: "[&BGMAAAA=]"
				},
				direction: "8点 富矿+矿"
			},
			{
				name: "伐木者",
				waypoint: {
					name: "铁拳传送点",
					chatlink: "[&BNMCAAA=]"
				},
				direction: "6点"
			},
			{
				name: "收割者",
				waypoint: {
					name: "蛇妖之祸避难所传送点",
					chatlink: "[&BEABAAA=]"
				},
				direction: "11点"
			},
		]
	},
	{
		name: "欧尔",
		children: [{
				name: "景观点",
				waypoint: {
					name: "三体堡垒传送点",
					chatlink: "[&BO4CAAA=]"
				},
				direction: "9点飞船头微远"
			},
			{
				name: "采矿者",
				waypoint: {
					name: "干预者传送点",
					chatlink: "[&BB4DAAA=]"
				},
				direction: "附近3矿+11点微远2矿 暂定"
			},
			{
				name: "伐木者",
				waypoint: {
					name: "干预者传送点",
					chatlink: "[&BB4DAAA=]"
				},
				direction: "附近一周"
			},
			{
				name: "收割者",
				waypoint: {
					name: "基座林传送点",
					chatlink: "[&BFgGAAA=]"
				},
				direction: "1点"
			},
		]
	},
	{
		name: "迈古玛荒野",
		children: [{
				name: "景观点",
				waypoint: {
					name: "决心营地传送点",
					chatlink: ""
				},
				direction: "营地内"
			},
			{
				name: "采矿者",
				waypoint: {
					name: "决心营地传送点",
					chatlink: ""
				},
				direction: "七点，营地外"
			},
			{
				name: "伐木者",
				waypoint: {
					name: "决心营地传送点",
					chatlink: ""
				},
				direction: "暂定"
			},
			{
				name: "收割者",
				waypoint: {
					name: "决心营地传送点",
					chatlink: ""
				},
				direction: "暂定"
			},
		]
	},
];