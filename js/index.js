FastClick.attach(document.body);
var mainView = new Vue({
	el: "#root",
	data: {
		tabIndex: 0,
		bossTimeList: [],
		dailyLocation: dailyLocation,
		waypoint: waypoint,
		waypointFilter: '',
		nowTime: formatTimeToStr(new Date()),
		dayOrNight: '',
	},
	mounted: function() {
		var T = this;
		T.getBossTimeData();
		T.dayOrNight = dayOrNight(T.nowTime);
		this.timmer = setInterval(function() {
			T.getBossTimeData();
			T.nowTime = formatTimeToStr(new Date());
			T.dayOrNight = dayOrNight(T.nowTime);
		}, 1000);
	},
	destroyed: function() {
		clearInterval(this.timmer);
	},
	methods: {
		getNearestTime: function(timeList) {
			var now = new Date();
			var nowTimeStr = formatTimeToStr(now);
			var nowSeconds = formatTimeToSeconds(nowTimeStr);
			var nextTime = '明' + timeList[0]; // 默认为第一个时间，如果不赋值，说明这是明天的了
			var prevTime = timeList[timeList.length - 1]; // 默认为最后一个时间，如果不赋值，则说明已经没有下次刷新了
			for(var i = 0; i < timeList.length; i++) {
				var bossTimeSeconds = formatTimeToSeconds(timeList[i]);
				var delta = bossTimeSeconds - nowSeconds;
				if(delta >= 0) {
					nextTime = timeList[i];
					prevTime = timeList[i - 1];
					break;
				}
			}
			return {
				nextTime: nextTime,
				prevTime: prevTime
			};
		},
		getBossTimeData: function() {
			var T = this;
			var bList = [];
			for(k in bossTimeList) {
				var timeObj = T.getNearestTime(bossTimeList[k].time);
				bossTimeList[k].timeObj = timeObj;
				bList.push(bossTimeList[k]);
			}
			//按照nextTime排序之后显示
			T.bossTimeList = _.sortBy(bList, function(obj) {
				return obj.timeObj.nextTime;
			});
		},
		changeTab: function(tabIndex) {
			this.tabIndex = tabIndex;
		}
	}
});