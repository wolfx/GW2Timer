var worldBoss = [{
	"name": "boss1",
	"c_name": "斯瓦尼亚萨满",
	"time": timeListGenerator("00:15:00", 2),
	"waypoint": {
		"name": "文德里克的山庄传送点",
		"chatlink": "[&BMIDAAA=]",
	},
}, {
	"name": "boss2",
	"c_name": "巨型丛林地虫",
	"time": timeListGenerator("01:15:00", 2),
	"waypoint": {
		"name": "暮光之根传送点",
		"chatlink": "[&BEEFAAA=]",
	},
}, {
	"name": "boss3",
	"c_name": "暗影巨兽",
	"time": timeListGenerator("01:45:00", 2),
	"waypoint": {
		"name": "迷沼避难所传送点",
		"chatlink": "[&BPcAAAA=]",
	},
}, {
	"name": "boss4",
	"c_name": "火焰元素",
	"time": timeListGenerator("00:45:00", 2),
	"waypoint": {
		"name": "幸存者营地传送点",
		"chatlink": "[&BEcAAAA=]",
	},
}, {
	"name": "boss5",
	"c_name": "进化巨型丛林地虫",
	"time": [
		"01:00:00",
		"05:00:00",
		"09:00:00",
		"12:00:00",
		"16:00:00",
		"20:30:00"
	],
	"waypoint": {
		"name": "峡口驻守站传送点",
		"chatlink": "[&BKoBAAA=]",
	},
}, {
	"name": "boss6",
	"c_name": "莫迪尔沃尔格斯（人马）",
	"time": timeListGenerator("00:30:00", 3),
	"waypoint": {
		"name": "偶蹄传送点",
		"chatlink": "[&BLAAAAA=]",
	},
}, {
	"name": "boss7",
	"c_name": "泰达·科文顿",
	"time": timeListGenerator("02:00:00", 3),
	"waypoint": {
		"name": "笑鸥传送点",
		"chatlink": "[&BKgBAAA=]",
	},
}, {
	"name": "boss8",
	"c_name": "碎裂巨兽",
	"time": timeListGenerator("00:00:00", 3),
	"waypoint": {
		"name": "灼热盆地传送点",
		"chatlink": "[&BE4DAAA=]",
	},
}, {
	"name": "boss9",
	"c_name": "吞噬托（僵尸龙）",
	"time": [
		"00:00:00",
		"04:00:00",
		"08:00:00",
		"11:00:00",
		"15:00:00",
		"19:30:00"
	],
	"waypoint": {
		"name": "破碎海岸传送点",
		"chatlink": "[&BNABAAA=]",
	},
}, {
	"name": "boss10",
	"c_name": "超能毁灭者",
	"time": timeListGenerator("02:30:00", 3),
	"waypoint": {
		"name": "漩涡传送点",
		"chatlink": "[&BM0CAAA=]",
	},
}, {
	"name": "boss11",
	"c_name": "魔像马克II型",
	"time": timeListGenerator("01:00:00", 3),
	"waypoint": {
		"name": "旧锤据点传送点",
		"chatlink": "[&BNQCAAA=]",
	},
}, {
	"name": "boss12",
	"c_name": "卓玛之爪",
	"time": timeListGenerator("01:30:00", 3),
	"waypoint": {
		"name": "地震传送点",
		"chatlink": "[&BHoCAAA=]",
	},
}, {
	"name": "boss13",
	"c_name": "喀壳虫女王",
	"time": [
		"03:00:00",
		"07:00:00",
		"10:00:00",
		"14:00:00",
		"18:30:00",
		"23:00:00"
	],
	"waypoint": {
		"name": "喀壳营地传送点",
		"chatlink": "[&BNUGAAA=]",
	},
}];

var s3World = [{
	"name": "test",
	"c_name": "苍翠夜间BOSS",
	"time": timeListGenerator("00:10:00", 2),
	"waypoint": {
		"name": "苍翠边界",
	}
},{
	"name": "test",
	"c_name": "苍翠前哨（蜥蜴）",
	"time": timeListGenerator("00:30:00", 2), // 双数30分开始做前置，单数45分结束 
	"waypoint": {
		"name": "苍翠边界",
	}
}, {
	"name": "test",
	"c_name": "魔径异常体（跑男）",
	"time": timeListGenerator("00:20:00", 2),
	"waypoint": {
		"name": "钢铁平原",
	}
}, {
	"name": "test",
	"c_name": "查克虫王",
	"time": timeListGenerator("00:30:00", 2),
	"waypoint": {
		"name": "缠藤深渊",
	}
}, {
	"name": "test",
	"c_name": "八爪藤（四门）",
	"time": timeListGenerator("01:00:00", 2),
	"waypoint": {
		"name": "赤金盆地",
	}
}, {
	"name": "test",
	"c_name": "莫德摩斯（巨龙）",
	"time": timeListGenerator("01:30:00", 2),
	"waypoint": {
		"name": "巨龙阵地",
	}
}, ];

var s4World = [{
		"name": "test",
		"c_name": "突袭：攻城",
		"time": timeListGenerator("01:45:00", 2),
		"waypoint": {
			"name": "伊斯坦领域",
		}
	},
	{
		"name": "test",
		"c_name": "突袭：腐蚀新月",
		"time": timeListGenerator("00:25:00", 2),
		"waypoint": {
			"name": "伊斯坦领域",
		}
	},
	{
		"name": "test",
		"c_name": "水晶绿洲：赌场",
		"time": timeListGenerator("00:05:00", 2),
		"waypoint": {
			"name": "水晶绿洲",
		}
	},
	{
		"name": "test",
		"c_name": "巨蛇之怒",
		"time": timeListGenerator("00:00:00", 1.5),
		"waypoint": {
			"name": "瓦比领域",
			"chatlink": "[&BHQKAAA=]",
		}
	},
];

var bossTimeList = _.union(worldBoss, s3World, s4World);