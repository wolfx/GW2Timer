使用须知：传送点列表来自网络+66翻译。目前只有老图传送点，新传送点看心情之后翻。格式看心情之后整理。
激战2传送点wiki：
【https://wiki.guildwars2.com/wiki/Chat_link_format/0x04_codes/Ascalon】

一、五大主城加狮子拱门
1、狮子拱门
[&BAwEAAA=]  【运河区传送点】
[&BA0EAAA=]  【走私贩传送点】
[&BA4EAAA=]  【血岸区传送点】
[&BA8EAAA=]  【利爪岛运输站传送点】
[&BBAEAAA=]  【商人广场传送点】
[&BBEEAAA=]  【星门广场传送点】
[&BC0EAAA=]  【怪石岩架传送点】
[&BC4EAAA=]  【西区传送点】
[&BC8EAAA=]  【避风港传送点】
[&BDAEAAA=]  【马尼纳堡垒传送点】
[&BDEEAAA=]  【东区传送点】
[&BDIEAAA=]  【外城区传送点】
[&BDMEAAA=]  【远岸区传送点】
2、黑烟壁垒【夏尔】
[&BKQDAAA=]  【孤斗士传送点】
[&BKUDAAA=]  【聚集地传送点】
[&BKYDAAA=]  【记录传送点】
[&BKcDAAA=]  【工厂区传送点】
[&BKgDAAA=]  【底耶沙大门传送点】
[&BKkDAAA=]  【北风之湖传送点】
[&BKoDAAA=]  【灾祸传送点】
[&BKsDAAA=]  【英雄区传送点】
[&BKwDAAA=]  【凛之废墟传送点】
[&BK0DAAA=]  【统领传送点】
[&BDcEAAA=]  【废料传送点】
[&BCkHAAA=]  【诺兰尼闹鬼地传送点】
3、霍布雷克【诺恩】
[&BIUDAAA=]  【全力之核传送点】
[&BIYDAAA=]  【商贸集市传送点】
[&BIcDAAA=]  【豹灵传送点】
[&BIgDAAA=]  【鸦灵传送点】
[&BIkDAAA=]  【熊灵传送点】
[&BIoDAAA=]  【狼灵传送点】
[&BIsDAAA=]  【避难岩传送点】
[&BIwDAAA=]  【传说传送点】
[&BI0DAAA=]  【东部哨站传送点】
[&BI4DAAA=]  【南部哨站传送点】
[&BI8DAAA=]  【皮塔传送点】
[&BJADAAA=]  【英雄罗盘传送点】
[&BNUDAAA=]  【大酒馆传送点】
[&BAgFAAA=]  【上层露台传送点】
4、拉塔索姆【阿苏拉】
[&BBMEAAA=]  【度量庭院传送点】
[&BBQEAAA=]  【占星庭院传送点】
[&BLQEAAA=]  【魔术庭院传送点】
[&BLUEAAA=]  【孵化传送点】
[&BLYEAAA=]  【会记学传送点】
[&BLcEAAA=]  【学徒传送点】
[&BLgEAAA=]  【研究传送点】
[&BLkEAAA=]  【辅助传送点】
[&BAcFAAA=]  【港口传送点】
5、圣林之地【希尔瓦里】
[&BBIEAAA=]  【卡勒顿传送点】
[&BLoEAAA=]  【上林区传送点】
[&BLsEAAA=]  【审计平台传送点】
[&BLwEAAA=]  【罗南传送点】
6、神佑之城【人类】
[&BCMDAAA=]  【德维娜传送点】
[&BCQDAAA=]  【古兰斯传送点】
[&BCUDAAA=]  【克米尔传送点】
[&BCYDAAA=]  【丽莎传送点】
[&BCcDAAA=]  【梅兰朵传送点】
[&BCgDAAA=]  【巴萨泽传送点】
[&BCkDAAA=]  【皇宫传送点】
[&BCoDAAA=]  【平民区传送点】
[&BCsDAAA=]  【鲁雷克顿传送点】
[&BCwDAAA=]  【皇冠高阁传送点】
[&BC0DAAA=]  【奥珊传送点】
[&BC4DAAA=]  【萨尔玛传送点】
[&BP4EAAA=]  【内阁传送点】
二、阿斯卡隆地区【夏尔】
1、阿什福得平原
[&BH8BAAA=]  【维尔之门传送点】
[&BIABAAA=]  【烟囱镇传送点】
[&BIEBAAA=]  【灰钢军械库传送点】
[&BIIBAAA=]  【殉难者传送点】
[&BIMBAAA=]  【塔帕斯点传送点】
[&BIQBAAA=]  【阿什福得传送点】
[&BIUBAAA=]  【阿多利亚传送点】
[&BIYBAAA=]  【阿斯卡隆传送点】
[&BIcBAAA=]  【阿斯卡隆城传送点】
[&BIgBAAA=]  【望崖塔传送点】
[&BIkBAAA=]  【裂暮眺望台传送点】
[&BIoBAAA=]  【钢铁码头船厂传送点】
[&BJcDAAA=]  【弗里塔斯传送点】
[&BJgDAAA=]  【德西姆斯据点传送点】
[&BJkDAAA=]  【灵魂猎手传送点】
[&BMcDAAA=]  【钳爪传送点】
[&BMgDAAA=]  【相位传送点】
[&BPgGAAA=]  【朗姆庄园】
2、底耶沙高地
[&BNkAAAA=]  【夏尔之门避难所传送点】
[&BNoAAAA=]  【诅咒泥地传送点】
[&BNsAAAA=]  【血锯作坊传送点】
[&BNwAAAA=]  【洛汉圣泉传送点】
[&BN0AAAA=]  【纳格林传送点】
[&BN4AAAA=]  【诺兰传送点】
[&BF4BAAA=]  【古门传送点】
[&BF8BAAA=]  【屠夫区传送点】
[&BGABAAA=]  【夏拉迪斯营地传送点】
[&BGEBAAA=]  【损毁之墙传送点】
[&BGIBAAA=]  【焚炉传送点】
[&BGMBAAA=]  【奈姆斯林传送点】
[&BGQBAAA=]  【泻水传送点】
[&BJoDAAA=]  【黎明庄园传送点】
[&BMQDAAA=]  【血崖传送点】
[&BMUDAAA=]  【圣所传送点】
[&BMYDAAA=]  【红刃作坊传送点】
[&BMkDAAA=]  【死人岩传送点】
[&BEIEAAA=]  【黑刃传送点】
3、废墟原野
[&BNMAAAA=]  【鹰门传送点】
[&BNQAAAA=]  【亡刃传送点】 
[&BNUAAAA=]	 【至高传送点】 
[&BNYAAAA=]  【泰勒营地传送点】 
[&BNcAAAA=]  【阴晦传送点】
[&BNgAAAA=]  【罗斯科营地传送点】 
[&BEoBAAA=]  【斯科尔营地传送点】
[&BEsBAAA=]  【哈里特矿井传送点】
[&BEwBAAA=]  【怒牙传送点】 
[&BE0BAAA=]  【秃鹰传送点】 
[&BE4BAAA=]  【吞血哨站残骸传送点】 
[&BE8BAAA=]  【食人魔传送点】
[&BFABAAA=]  【无望之门传送点】 
[&BFEBAAA=]  【堕天使要塞传送点】 
[&BDwEAAA=]  【观测者传送点】
[&BD0EAAA=]  【红隼传送点】 
[&BD4EAAA=]  【破雷传送点】 
4、裂脊草原
[&BPkBAAA=]  【最后的威士忌酒吧传送点】
[&BPoBAAA=]  【铁眼传送点】
[&BPsBAAA=]  【图莫克传送点】
[&BPwBAAA=]  【孪生姐妹传送点】
[&BP0BAAA=]  【贝亨传送点】
[&BP4BAAA=]  【荒原传送点】
[&BP8BAAA=]  【守护石传送点】
[&BAACAAA=]  【伦克栅栏传送点】
[&BAECAAA=]  【克伦达之地传送点】
[&BAICAAA=]  【裂齿王座传送点】
[&BAMCAAA=]  【燃灼传送点】
[&BAQCAAA=]  【烙印之景传送点】
[&BAUCAAA=]  【劫难庇护所传送点】
[&BE4DAAA=]  【灼热盆地传送点】
[&BE8DAAA=]  【加斯托山峡传送点】
[&BFADAAA=]  【琪纳堡传送点】
[&BFEDAAA=]  【破浪堡传送点】
[&BFIDAAA=]  【烟尘之路传送点】
5、钢铁平原
[&BOIBAAA=]  【潮爪传送点】
[&BOMBAAA=]  【血鳍湖传送点】
[&BOQBAAA=]  【派肯遗迹传送点】
[&BOUBAAA=]  【战犬村传送点】
[&BOYBAAA=]  【恶徒传送点】
[&BOcBAAA=]  【鳞爪村传送点】
[&BOgBAAA=]  【亮皮营地传送点】
[&BOkBAAA=]  【烙印哨站营地传送点】
[&BOoBAAA=]  【毒蛇溪谷传送点】
[&BOsBAAA=]  【兽牙星镇传送点】
[&BOwBAAA=]  【壁垒传送点】
[&BO0BAAA=]  【观火营地传送点】
[&BO4BAAA=]  【荒林传送点】
[&BO8BAAA=]  【格罗斯托栅栏传送点】
6、炎心高地
[&BBYCAAA=]  【萨提传送点】
[&BBcCAAA=]  【生铁传送点】
[&BBgCAAA=]  【狂风传送点】
[&BBkCAAA=]  【断离缺口传送点】
[&BBoCAAA=]  【断牙传送点】
[&BBsCAAA=]  【浩劫传送点】
[&BBwCAAA=]  【维迪乌斯兵营传送点】
[&BB0CAAA=]  【叛变者传送点】
[&BB4CAAA=]  【铁腕传送点】
[&BB8CAAA=]  【蜿蜒传送点】
[&BCACAAA=]  【冰矛传送点】
[&BCECAAA=]  【雪岭营传送点】
[&BCICAAA=]  【沃格斯要塞传送点】
[&BCMCAAA=]  【无望传送点】
[&BCQCAAA=]  【斯内科兵营传送点】
[&BCUCAAA=]  【斯莫传送点】
[&BCYCAAA=]  【守护者传送点】
[&BEAFAAA=]  【烈焰壁垒传送点】

三、希瓦雪山 【诺恩】
1、旅者丘陵 
[&BHIBAAA=]【弃儿传送点】
[&BHMBAAA=]【英雄大会传送点】
[&BHQBAAA=]【号令传送点】
[&BHUBAAA=]【暗壑崖传送点】
[&BHYBAAA=]【泰戈恩传送点】
[&BHcBAAA=]【泽里奇温泉传送点】
[&BHgBAAA=]【哈温特传送点】
[&BHkBAAA=]【十字路避难所传送点】
[&BHoBAAA=]【晨曦传送点】
[&BHsBAAA=]【石牦牛传送点】
[&BHwBAAA=]【荒僻山谷传送点】
[&BH0BAAA=]【双峰避难所传送点】
[&BH4BAAA=]【文德里克的山庄传送点】
[&BMEDAAA=]【隐龙洞窟传送点】
[&BMIDAAA=]【科伦纳克的山庄传送点】
[&BMMDAAA=]【穴居人海湾传送点】
[&BAEEAAA=]【奥森福德传送点】
2、漂流雪境 
[&BLMAAAA=]【斯卡顿传送点】
[&BLQAAAA=]【罗纳传送点】
[&BLUAAAA=]【天道避难所传送点】
[&BLYAAAA=]【失独之悲传送点】
[&BLcAAAA=]【学者之角传送点】
[&BLgAAAA=]【铁瀑湖传送点】
[&BLkAAAA=]【雪落避难所传送点】
[&BLoAAAA=]【炽天使先驱传送点】
[&BLsAAAA=]【托斯德山庄传送点】
[&BLwAAAA=]【流亡传送点】
[&BL0AAAA=]【尼奥尔德山庄传送点】
[&BL4AAAA=]【索德海姆山庄传送点】
[&BL8AAAA=]【雪鹰栖所传送点】
[&BMAAAAA=]【掠夺者传送点】
[&BMEAAAA=]【枭灵传送点】
[&BMIAAAA=]【伯达戈山庄传送点】
[&BL8DAAA=]【冰封之地传送点】
[&BMADAAA=]【瓦斯湖传送点】
[&BP8GAAA=]【安格瓦藏宝地传送点】
3、罗纳通道 
[&BOUAAAA=]【范齐尔营地传送点】
[&BOYAAAA=]【恶魔血口传送点】
[&BOcAAAA=]【融冬传送点】
[&BOgAAAA=]【错湖传送点】
[&BOkAAAA=]【德曼休会传送点】
[&BOoAAAA=]【哀恸传送点】
[&BOsAAAA=]【雷鸣之角传送点】
[&BOwAAAA=]【南托传送点】
[&BJYBAAA=]【古特拉的农庄传送点】
[&BJcBAAA=]【石墟传送点】
[&BJgBAAA=]【尖峰围地传送点】
[&BJkBAAA=]【裂雾传送点】
[&BFEGAAA=]【冰魔传送点】
[&BFIGAAA=]【避难峰传送点】
[&BFMGAAA=]【阿弗戈传送点】
[&BFQGAAA=]【瀑布桥传送点】
[&BDEHAAA=]【错河传送点】
4、掘洞悬崖 
[&BFYCAAA=]【苦难传送点】
[&BFcCAAA=]【霜地传送点】
[&BFgCAAA=]【杜修传送点】
[&BFkCAAA=]【七棵松传送点】
[&BFoCAAA=]【灰白之路传送点】
[&BFsCAAA=]【钢铁之臂传送点】
[&BFwCAAA=]【托兰山谷传送点】
[&BF0CAAA=]【哈斯达伦聚地传送点】
[&BF4CAAA=]【聚地试验场传送点】
[&BF8CAAA=]【广漠旷野传送点】
[&BGACAAA=]【诺托故障传送点】
[&BGECAAA=]【山尾传送点】
[&BGICAAA=]【冰原传送点】
[&BGMCAAA=]【哈弗伦盆地传送点】
[&BGQCAAA=]【特弗伦传送点】
[&BGUCAAA=]【飞龙雪湖传送点】
[&BO4EAAA=]【花岗岩城垒传送点】
[&BD8FAAA=]【悲伤之拥传送点】
5、林线瀑布 
[&BEQCAAA=]【塔鲁斯传送点】
[&BEUCAAA=]【毒蛇传送点】
[&BEYCAAA=]【奥卡莉诺传送点】
[&BEcCAAA=]【游丝传送点】
[&BEgCAAA=]【鳞爪湖滨传送点】
[&BEkCAAA=]【陆行鸟禁湖传送点】
[&BEoCAAA=]【鲜血托领地传送点】
[&BEsCAAA=]【疾旋环流传送点】
[&BEwCAAA=]【帷幔城垛传送点】
[&BE0CAAA=]【奥格德传送点】
[&BE4CAAA=]【86号基地传送点】
[&BE8CAAA=]【净纸传送点】
[&BFACAAA=]【柔水河传送点】
[&BFECAAA=]【芦荟林传送点】
[&BFICAAA=]【科隆格传送点】
[&BFMCAAA=]【钢铁之幕传送点】
[&BFQCAAA=]【卡兰遗址传送点】
[&BFUCAAA=]【康考迪亚传送点】
[&BEYEAAA=]【斯洛卡尔传送点】
6、霜谷之音 
[&BHgCAAA=]【阿鲁顿传送点】
[&BHkCAAA=]【格洛耐传送点】
[&BHoCAAA=]【地震传送点】
[&BHsCAAA=]【星空小径传送点】
[&BHwCAAA=]【绝望泥潭传送点】
[&BH0CAAA=]【警戒传送点】
[&BH4CAAA=]【浮冰传送点】
[&BH8CAAA=]【迪莫提克传送点】
[&BIACAAA=]【双环村传送点】
[&BIECAAA=]【远空山庄传送点】
[&BIICAAA=]【山极峰传送点】
[&BIMCAAA=]【脊石营地传送点】
[&BIQCAAA=]【耗牛拐点传送点】
[&BIUCAAA=]【闪耀蓝冰传送点】
[&BIYCAAA=]【德拉克传送点】
[&BEMFAAA=]【海浪之誉传送点】




四、科瑞塔地区【人类】
1、女王谷
[&BO8AAAA=]【夏摩尔传送点】
[&BPAAAAA=]【麦田传送点】
[&BPEAAAA=]【要塞传送点】
[&BPIAAAA=]【渡口传送点】
[&BPMAAAA=]【菲尼传送点】
[&BPQAAAA=]【溪谷传送点】
[&BPUAAAA=]【心木小径营地传送点】
[&BPYAAAA=]【泥潭镇传送点】
[&BPcAAAA=]【沼泽避难所传送点】
[&BPgAAAA=]【科瑞塔传送点】
[&BPkAAAA=]【欧琼的伐木场传送点】
[&BPoAAAA=]【甲虫郡传送点】
[&BPsAAAA=]【郡守哨站传送点】
[&BPwAAAA=]【迷神传送点】
[&BEQDAAA=]【果园传送点】
[&BEUDAAA=]【斯卡维高原传送点】
2、凯席斯山
[&BAMAAAA=]【萨阿玛堡垒传送点】
[&BAQAAAA=]【霸主传送点】
[&BAYAAAA=]【哈拉坎传送点】
[&BAcAAAA=]【旅举者传送点】
[&BAgAAAA=]【德兰传送点】
[&BAoAAAA=]【埃里克贸易站传送点】
[&BAwAAAA=]【暗影之行据点传送点】
[&BBAAAAA=]【利维坦传送点】
[&BBEAAAA=]【暗伤传送点】
[&BBIAAAA=]【瑟拉波斯传送点】
[&BBMAAAA=]【临渊避难所传送点】
[&BBQAAAA=]【凯席斯避难所传送点】
[&BBUAAAA=]【洞穴据点营地传送点】
[&BBYAAAA=]【灰蹄营地传送点】
[&BLkDAAA=]【土木营地传送点】
[&BLoDAAA=]【裂口传送点】
3、甘达拉战区
[&BN8AAAA=]【旅行者山谷传送点】
[&BOAAAAA=]【右门卫传送点】
[&BOEAAAA=]【巨窟传送点】
[&BOIAAAA=]【欧格斯传送点】
[&BOMAAAA=]【丰饶之地传送点】
[&BOQAAAA=]【誓言海岸传送点】
[&BO0AAAA=]【枢纽避难所传送点】
[&BO4AAAA=]【冬季避难所传送点】
[&BIsBAAA=]【原初避难所传送点】
[&BIwBAAA=]【塔拉捷传送点】
[&BI0BAAA=]【血山传送点】
[&BI4BAAA=]【尼波平台传送点】
[&BI8BAAA=]【阿斯卡隆殖民地传送点】
[&BJABAAA=]【北部原野传送点】
[&BJEBAAA=]【苹果园传送点】
[&BJIBAAA=]【守夜人要塞传送点】
[&BJMBAAA=]【冰门传送点】
[&BJQBAAA=]【阿姆顿传送点】
[&BJsDAAA=]【狮桥传送点】
[&BMwDAAA=]【雪盲传送点】
[&BM0DAAA=]【双桅帆船传送点】
[&BAIEAAA=]【鲜血战场传送点】
4、哈拉希腹地
[&BKUAAAA=]【牧神传送点】
[&BKYAAAA=]【盾崖传送点】
[&BKcAAAA=]【炽天使平台传送点】
[&BKgAAAA=]【维乔娜集结点传送点】
[&BKkAAAA=]【灰格里塔传送点】
[&BKoAAAA=]【夜幕守卫传送点】
[&BKsAAAA=]【德美特拉传送点】
[&BKwAAAA=]【复苏野营传送点】
[&BK0AAAA=]【路障营地传送点】
[&BK4AAAA=]【切布莎眺望台传送点】
[&BK8AAAA=]【桥头哨站传送点】
[&BLAAAAA=]【枢纽营地传送点】
[&BLEAAAA=]【偶蹄传送点】
[&BLIAAAA=]【阿卡传送点】
[&BMMAAAA=]【阿卡利安传送点】

5、血潮海岸
[&BKMBAAA=]【亚盛海角传送点】
[&BKQBAAA=]【悲音传送点】
[&BKUBAAA=]【暴风崖传送点】
[&BKYBAAA=]【湿地守卫避难所传送点】
[&BKcBAAA=]【雷曼达盐沼传送点】
[&BKgBAAA=]【笑鸥传送点】
[&BKkBAAA=]【结界营地传送点】
[&BKoBAAA=]【峡口驻守站传送点】
[&BKsBAAA=]【失事传送点】
[&BKwBAAA=]【沼岸营地传送点】
[&BK0BAAA=]【悲恸传送点】
[&BK4BAAA=]【卡斯特沃传送点】
[&BK8BAAA=]【杰拉克传送点】
[&BLABAAA=]【轻语之意传送点】
[&BAsEAAA=]【死路传送点】


五、晦暗海岸【阿苏拉和希尔瓦里】
1、卡勒顿之森
[&BDQBAAA=]【星空传送点】
[&BDUBAAA=]【摩根传送点】
[&BDYBAAA=]【星空壁垒传送点】
[&BDcBAAA=]【拾穗者峡谷传送点】
[&BDgBAAA=]【布里迪格瞭望台传送点】
[&BDkBAAA=]【山际传送点】
[&BDoBAAA=]【玛邦传送点】
[&BDsBAAA=]【卡瑟尔镇传送点】
[&BDwBAAA=]【卡勒顿避难所传送点】
[&BD0BAAA=]【泰坦阶梯传送点】
[&BD4BAAA=]【法利亚斯村传送点】
[&BD8BAAA=]【安文村传送点】
[&BEABAAA=]【蛇妖之祸避难所传送点】
[&BEEBAAA=]【维琪米尔传送点】
[&BEIBAAA=]【守望者树林传送点】
[&BEwDAAA=]【雄狮守卫驿站传送点】
[&BEEFAAA=]【暮光之根传送点】
[&BP4FAAA=]【斯雷弗传送点】
2、度量领域
[&BEAAAAA=]【索兰德拉传送点】
[&BEEAAAA=]【杰兹塔瀑布传送点】
[&BEIAAAA=]【阿克野原传送点】
[&BEMAAAA=]【拉纳着陆点设施传送点】
[&BEQAAAA=]【医护避难所传送点】
[&BEUAAAA=]【己烷提纯实验室传送点】
[&BEYAAAA=]【幸存者营地传送点】
[&BEcAAAA=]【子午线传送点】
[&BEgAAAA=]【渴求之地传送点】
[&BK4EAAA=]【古代魔像铸造厂传送点】
[&BK8EAAA=]【海湾传送点】
[&BLAEAAA=]【蚁丘传送点】
[&BLEEAAA=]【秘行托领地传送点】
[&BLIEAAA=]【贪吃托传送点】
[&BLMEAAA=]【动脉角传送点】
[&BPcEAAA=]【水能开发团传送点】
3、布里斯班野地
[&BFwAAAA=]【警戒源传送点】
[&BF0AAAA=]【维登传送点】
[&BF4AAAA=]【隧道传送点】
[&BF8AAAA=]【山坡传送点】
[&BGAAAAA=]【城东传送点】
[&BGEAAAA=]【布里提尼传送点】
[&BGIAAAA=]【炽天使观察者传送点】
[&BGMAAAA=]【盗匪荒地传送点】
[&BGQAAAA=]【三心传送点】
[&BGUAAAA=]【乌塔超魔会传送点】
[&BHUAAAA=]【姆洛特坑洞传送点】
[&BHYAAAA=]【幽腾传送点】
[&BPkGAAA=]【空间关系实验室传送点】

六、蒸汽山脉
1、闪萤沼泽
[&BMQBAAA=]【欧瓦幽谷传送点】
[&BMUBAAA=]【卡登斯壁垒传送点】
[&BMYBAAA=]【布拉吉农场传送点】
[&BMcBAAA=]【盐溢传送点】
[&BMgBAAA=]【水居托神圣领地传送点】
[&BMkBAAA=]【大洋峡传送点】
[&BMoBAAA=]【佛瓦传送点】
[&BMsBAAA=]【透德之头传送点】
[&BMwBAAA=]【火蛙传送点】
[&BM0BAAA=]【黑天传送点】
[&BM4BAAA=]【淡盐水传送点】
[&BM8BAAA=]【阿利姆传送点】
[&BNABAAA=]【破碎海岸传送点】
[&BNEBAAA=]【蹼噜努传送点】
[&BE0DAAA=]【旱地村庄传送点】
[&BLMDAAA=]【蔷薇壁垒传送点】
[&BLQDAAA=]【泥沼传送点】
2、漩涡山
[&BMcCAAA=]【黑暗溪谷传送点】
[&BMgCAAA=]【格沃冉传送点】
[&BMkCAAA=]【准则传送点】
[&BMoCAAA=]【破火壁垒传送点】
[&BMsCAAA=]【特异勃论传送点】
[&BMwCAAA=]【吟游诗人传送点】
[&BM0CAAA=]【漩涡传送点】
[&BM4CAAA=]【灰烬传送点】
[&BM8CAAA=]【阿瓦兰传送点】
[&BNACAAA=]【断箭传送点】
[&BNECAAA=]【牛辔岛传送点】
[&BNICAAA=]【欧文岛传送点】
[&BNMCAAA=]【铁拳传送点】
[&BNQCAAA=]【旧锤据点传送点】
[&BNUCAAA=]【审判传送点】
[&BNYCAAA=]【岩浆传送点】
[&BEIFAAA=]【永恒熔炉传送点】


七、欧尔遗迹 
1、浩劫海峡 
[&BOwCAAA=]【荆棘林道传送点】
[&BO0CAAA=]【烽火台传送点】
[&BO4CAAA=]【三体堡垒传送点】
[&BO8CAAA=]【荆棘小径传送点】
[&BPACAAA=]【雷首传送点】
[&BPECAAA=]【破碎岛传送点】
[&BPICAAA=]【薄暮钟传送点】
[&BPMCAAA=]【皇家广场传送点】
[&BPQCAAA=]【钢爪传送点】
[&BPUCAAA=]【美术馆传送点】
[&BPYCAAA=]【征服码头传送点】
[&BPcCAAA=]【孤寂营地传送点】
[&BPgCAAA=]【绝壁传送点】
[&BPkCAAA=]【席娜里斯传送点】
[&BPoCAAA=]【辉煌胜利传送点】
[&BPsCAAA=]【哨卫传送点】
[&BNIEAAA=]【集结传送点】
[&BFgGAAA=]【基座林传送点】
[&BFkGAAA=]【幽径传送点】
[&BOUGAAA=]【恐怖浅海传送点】

2、马尔科之跃 
[&BKYCAAA=]【帕加传送点】
[&BKcCAAA=]【多里克传送点】
[&BKgCAAA=]【荒芜凹地传送点】
[&BKkCAAA=]【柱廊传送点】
[&BKoCAAA=]【丽丝峡谷传送点】
[&BKsCAAA=]【联盟传送点】
[&BKwCAAA=]【雷恩传送点】
[&BK0CAAA=]【丽莎传送点】
[&BK4CAAA=]【瘟疫拱门传送点】
[&BK8CAAA=]【低语传送点】
[&BLACAAA=]【暴风雨传送点】
[&BLECAAA=]【反法术传送点】
[&BLICAAA=]【光芒传送点】
3、诅咒海岸 
[&BBcDAAA=]【追猎者小径传送点】
[&BBgDAAA=]【研发所传送点】
[&BBkDAAA=]【忏悔者小径传送点】
[&BBoDAAA=]【盖夫伯恩传送点】
[&BBsDAAA=]【新翠传送点】
[&BBwDAAA=]【庇护之门传送点】
[&BB0DAAA=]【乔法斯传送点】
[&BB4DAAA=]【干预者传送点】
[&BB8DAAA=]【锚地传送点】
[&BCADAAA=]【亚拉传送点】
[&BCEDAAA=]【影落城堡传送点】
[&BCIDAAA=]【弑梦传送点】
[&BOQGAAA=]【沉船岩传送点】

八、南阳海湾
1、南阳海湾
[&BNAGAAA=]【雄狮岬角传送点】
[&BNIGAAA=]【荣誉浪尖传送点】
[&BNUGAAA=]【明珠屿传送点】
[&BNcGAAA=]【喀壳营地传送点】
[&BNgGAAA=]【欧文避难所传送点】
[&BNwGAAA=]【齐尔前哨传送点】

