const lineReader = require('line-reader');
const fs = require('fs');

let typeName = "";
const result = {};
lineReader.eachLine('data.txt', function (line, last) {
  // console.log(line);
  if ("1234567890".indexOf(line[0]) >= 0) {
    // 数字开头
    typeName = line.split("、")[1];
    console.log(typeName)
  } else if ("一二三四五六七八九十".indexOf(line[0]) == -1 && typeName) {
    if (result[typeName] === undefined) {
      result[typeName] = [];
    }
    let lineArr = line.split("【");
    if (lineArr.length > 1) {
      result[typeName].push({
        chatlink: lineArr[0],
        name: lineArr[1].substring(0, lineArr[1].length - 1),
      })
    }
  }

  if (last) {
    console.log(result)
    const dataArray = [];
    for (let k in result) {
      dataArray.push({
        name: k,
        children: result[k]
      });
    }
    fs.writeFile('data.json', JSON.stringify(dataArray), function (err) {
      if (err) throw err;
      console.log('Saved successfully');
    });
  }
});

